package com.projet.tournoi;


import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TournoiApplication {

	 //Application  Context
	
    public static void main(String[] args) {
        SpringApplication.run(TournoiApplication.class, args);
    }

   
}
