package com.projet.tournoi.entities;

import java.util.Collection;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;



@Entity
@Data
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode
@ToString
public class Arbitre {
	@Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long idArbitre;
    @Column
    private String nom;
    @Column
    private String nationalite;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "arbitre")
    Collection<Matche> matches;

	

}
