package com.projet.tournoi.entities;

import java.util.Collection;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;

import org.hibernate.annotations.ManyToAny;

import com.fasterxml.jackson.annotation.JsonManagedReference;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@NoArgsConstructor
@AllArgsConstructor
@Data
public class Equipe {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long idEquipe;
	@Column
	private String nomEquipe;
	@Column
	private String pays;

	@OneToMany(cascade = CascadeType.ALL, mappedBy = "equipe")
	Collection<Joueur> joueurs;

	@ManyToAny(metaColumn = @Column(name = "matches", length = 20), fetch = FetchType.LAZY)
	Collection<Matche> matches;

	
}
