package com.projet.tournoi.entities;

import java.util.Calendar;
import java.util.Collection;
import java.util.Date;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;

import org.hibernate.annotations.ManyToAny;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;



@Entity
@NoArgsConstructor
@AllArgsConstructor
@Data 
public class Matche  {
	 @Id
	 @GeneratedValue(strategy = GenerationType.IDENTITY)
	 private Long idMatch;
	 @Column
	 private Date dateMatch;
	 @Column
	 private Calendar heureMatch;
	
	@ManyToAny(metaColumn = @Column(name = "equipes", length = 2), fetch = FetchType.LAZY)
    Collection<Equipe> equipes;
    @ManyToOne(cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    Stade stade;
    @ManyToOne(cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    Arbitre arbitre;


	
}
