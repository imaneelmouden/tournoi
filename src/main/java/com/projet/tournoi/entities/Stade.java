package com.projet.tournoi.entities;

import java.util.Collection;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@NoArgsConstructor
@AllArgsConstructor
@Data
public class Stade {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long idStade;
	@Column
	private String nomStade;
	@Column
	private String ville;
	
	@OneToMany(cascade = CascadeType.ALL, mappedBy = "stade")
	Collection<Matche> matches;

}
