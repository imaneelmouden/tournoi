package com.projet.tournoi.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.projet.tournoi.entities.Equipe;


@Repository
public interface EquipeRepository extends JpaRepository<Equipe, Long> {

}
