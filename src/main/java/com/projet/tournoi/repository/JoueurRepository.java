package com.projet.tournoi.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.projet.tournoi.entities.Joueur;

@Repository
public interface JoueurRepository extends JpaRepository<Joueur, Long> {

}
