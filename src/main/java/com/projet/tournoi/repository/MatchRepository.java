package com.projet.tournoi.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.projet.tournoi.entities.Matche;


@Repository
public interface MatchRepository extends JpaRepository<Matche, Long> {

}
