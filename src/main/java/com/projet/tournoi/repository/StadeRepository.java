package com.projet.tournoi.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.projet.tournoi.entities.Stade;



@Repository
public interface StadeRepository extends JpaRepository<Stade, Long> {

}
