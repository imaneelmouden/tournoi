package com.projet.tournoi.service.fd;

import java.util.Collection;

import com.projet.tournoi.entities.Arbitre;

public interface ArbitreService {
	Arbitre getArbitre (Long idArbitre);
	Arbitre ajouterArbitre (Arbitre a);
	Arbitre modifierArbitre (Arbitre a);
	void supprimerArbitre(Long idArbitre);
	Collection<Arbitre> getCollectionArbitre();
}
