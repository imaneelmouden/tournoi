package com.projet.tournoi.service.fd;

import java.util.Collection;

import com.projet.tournoi.entities.Equipe;

public interface EquipeService {

	Equipe getEquipe(Long idEquipe);
	Equipe ajouterEquipe(Equipe e);
	Equipe modifierEquipe(Equipe e);
	void supprimerEquipe(Long idEquipe);
	Collection<Equipe> getAll();
}
