package com.projet.tournoi.service.fd;

import java.util.Collection;

import com.projet.tournoi.entities.Joueur;

public interface JoueurService {
	Joueur getJoueur (Long idJoueur);
	Joueur ajouterJoueur(Joueur J);
	Joueur modifierJoueur (Joueur J);
	void supprimerJoueur (Long idJoueur);
	Collection <Joueur> getAll();
}
