package com.projet.tournoi.service.fd;

import java.util.Collection;

import com.projet.tournoi.entities.Matche;

public interface MatchService {
	Matche getMatch (Long idMatche);
	Matche ajoutertMatch (Matche M);
	Matche modifierMatch(Matche M);
	void supprimerMatche(Long idMatche);
	Collection <Matche> getAll();
}
