package com.projet.tournoi.service.fd;

import java.util.Collection;

import com.projet.tournoi.entities.Stade;

public interface StadeService {
	Stade getStade (Long idStade);
	Stade ajoutertStade(Stade S);
	Stade modifierStade(Stade S);
	void supprimerStade(Long idStade);
	Collection <Stade> getAll();
}
