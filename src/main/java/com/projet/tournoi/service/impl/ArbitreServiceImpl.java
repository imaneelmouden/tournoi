package com.projet.tournoi.service.impl;

import java.util.Collection;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.projet.tournoi.entities.Arbitre;
import com.projet.tournoi.repository.ArbitreRepository;
import com.projet.tournoi.service.fd.ArbitreService;

@Service
public class ArbitreServiceImpl implements ArbitreService {

	@Autowired
	ArbitreRepository arbitrerepository;

	@Override
	public Arbitre getArbitre(Long idArbitre) {
		return arbitrerepository.findById(idArbitre).get();
	}

	@Override
	public Arbitre ajouterArbitre(Arbitre a) {
		return arbitrerepository.save(a);
	}

	@Override
	public Arbitre modifierArbitre(Arbitre a) {
		return arbitrerepository.save(a);
	}

	@Override
	public void supprimerArbitre(Long idArbitre) {
		arbitrerepository.deleteById(idArbitre);
	}

	@Override
	public Collection<Arbitre> getCollectionArbitre() {
		return arbitrerepository.findAll();
	}
}
