package com.projet.tournoi.service.impl;

import java.util.Collection;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.projet.tournoi.entities.Equipe;
import com.projet.tournoi.repository.EquipeRepository;
import com.projet.tournoi.service.fd.EquipeService;
@Service
public class EquipeServiceImpl implements EquipeService{
@Autowired 
EquipeRepository equiperepository;

@Override
public Equipe getEquipe(Long idEquipe) {
	return equiperepository.findById(idEquipe).get();
}
@Override
public Equipe ajouterEquipe(Equipe e) {
	return equiperepository.save(e);
}
@Override
public Equipe modifierEquipe(Equipe e) {
	return equiperepository.save(e);
}
@Override
public void supprimerEquipe(Long idEquipe) {
	equiperepository.deleteById(idEquipe);
}
@Override
public Collection<Equipe> getAll(){
	return equiperepository.findAll();
}
}
