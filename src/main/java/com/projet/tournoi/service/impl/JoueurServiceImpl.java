package com.projet.tournoi.service.impl;

import java.util.Collection;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.projet.tournoi.entities.Joueur;
import com.projet.tournoi.repository.JoueurRepository;
import com.projet.tournoi.service.fd.JoueurService;
@Service
public class JoueurServiceImpl implements JoueurService{
	
@Autowired
JoueurRepository joueurrepository;
@Override
public Joueur getJoueur (Long idJoueur) {
	return joueurrepository.findById(idJoueur).get();
}
@Override
public Joueur ajouterJoueur(Joueur J) {
	return joueurrepository.save(J);
}
@Override
public Joueur modifierJoueur (Joueur J) {
	return joueurrepository.save(J);
}
@Override
public void supprimerJoueur (Long idJoueur) {
	joueurrepository.deleteById(idJoueur);
}
@Override
public Collection <Joueur> getAll(){
	return joueurrepository.findAll();
}

}
