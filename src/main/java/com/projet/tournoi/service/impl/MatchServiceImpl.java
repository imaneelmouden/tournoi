package com.projet.tournoi.service.impl;

import java.util.Collection;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.projet.tournoi.entities.Matche;
import com.projet.tournoi.repository.MatchRepository;
import com.projet.tournoi.service.fd.MatchService;
@Service
public class MatchServiceImpl implements MatchService{

@Autowired
MatchRepository matchrepository;

@Override
public Matche getMatch (Long idMatche) {
	return matchrepository.findById(idMatche).get();
}
@Override
public Matche ajoutertMatch (Matche M) {
	return matchrepository.save(M);
}
@Override
public Matche modifierMatch(Matche M) {
	return matchrepository.save(M);
}
@Override
public void supprimerMatche(Long idMatche) {
	matchrepository.deleteById(idMatche);
}
@Override
public Collection <Matche> getAll(){
	return matchrepository.findAll();
}
}
