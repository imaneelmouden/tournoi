package com.projet.tournoi.service.impl;

import java.util.Collection;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.projet.tournoi.entities.Stade;
import com.projet.tournoi.repository.StadeRepository;
import com.projet.tournoi.service.fd.StadeService;
@Service
public class StadeServiceImpl implements StadeService{

	@Autowired
	StadeRepository staderepository;

	@Override
	public Stade getStade (Long idStade) {
		return staderepository.findById(idStade).get();
	}
	@Override
	public Stade ajoutertStade(Stade S) {
		return staderepository.save(S);
	}
	@Override
	public Stade modifierStade(Stade S) {
		return staderepository.save(S);
	}
	@Override
	public void supprimerStade(Long idStade) {
		staderepository.deleteById(idStade);
	}
	@Override
	public Collection <Stade> getAll(){
		return staderepository.findAll();
	}

}
