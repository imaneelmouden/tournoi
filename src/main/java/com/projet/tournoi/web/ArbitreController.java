package com.projet.tournoi.web;





import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import com.projet.tournoi.entities.Arbitre;
import com.projet.tournoi.service.fd.ArbitreService;

import java.util.Collection;




@RestController
public class ArbitreController {
    @Autowired
    ArbitreService arbitreservice;
    @GetMapping("/arbitre/{id}")
    public Arbitre getArbitre(@PathVariable Long id) {
    	return arbitreservice.getArbitre(id);
    }

    
    @GetMapping("/arbitre")
    public Collection<Arbitre> getAllArbitre() {
        return arbitreservice.getCollectionArbitre();
    }

    @PostMapping("/arbitre")
    public Arbitre addArbitre(@RequestBody Arbitre a) {
        return arbitreservice.ajouterArbitre(a);

    }
    @PutMapping("/arbitre/{id}")
    public Arbitre updateArbitre(@RequestBody Arbitre a){
        return arbitreservice.modifierArbitre(a);
    }
    @DeleteMapping("/arbitre/{id}")
    public void deleteArbitre(@PathVariable Long id){
        arbitreservice.supprimerArbitre(id);
    }


}