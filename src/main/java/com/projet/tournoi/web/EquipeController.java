package com.projet.tournoi.web;

import java.util.Collection;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;


import com.projet.tournoi.entities.Equipe;
import com.projet.tournoi.service.fd.EquipeService;

@RestController
public class EquipeController {
	
  @Autowired
  EquipeService equipeservice;
	
  @GetMapping("/equipe/{id}")
  public Equipe getequipe(@PathVariable Long id) {
	  return equipeservice.getEquipe(id);
	 
 }
  
  @GetMapping("/equipe")
  public Collection<Equipe> getequipes(){
	  return equipeservice.getAll();
  }
  
  @PostMapping("/equipe")
  public Equipe addequipe(@RequestBody Equipe e) {
	  return equipeservice.ajouterEquipe(e);
  }
  
  @PutMapping("/equipe")
  public Equipe updateequipe(@RequestBody Equipe e) {
	 return equipeservice.modifierEquipe(e);
  }
  
  @DeleteMapping("/equipe/{id}")
  public void delelteequipe(@PathVariable Long id) {
	  equipeservice.supprimerEquipe(id);
  }

}
