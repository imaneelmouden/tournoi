package com.projet.tournoi.web;

import java.util.Collection;

import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.projet.tournoi.entities.Joueur;
import com.projet.tournoi.service.fd.JoueurService;


@RestController
public class JoueurController {
	@Autowired
	  JoueurService joueurservice;
		
	  @GetMapping("/joueur/{id}")
	  public Joueur getjoueur(@PathVariable Long id) {
		  return joueurservice.getJoueur(id);
		 
	 }
	  
	  @GetMapping("/joueur")
	  public Collection<Joueur> getjoueurs(){
		  return joueurservice.getAll();
	  }
	  
	  @PostMapping("/joueur")
	  public Joueur addjoueur(@RequestBody Joueur J) {
		  return joueurservice.ajouterJoueur(J);
	  }
	  
	  @PutMapping("/joueur")
	  public Joueur updatejoueur(@RequestBody Joueur J) {
		 return joueurservice.modifierJoueur(J);
	  }
	  
	  @DeleteMapping("/joueur/{id}")
	  public void deleltejoueur(@PathVariable Long id) {
		  joueurservice.supprimerJoueur(id);
	  }

	}


