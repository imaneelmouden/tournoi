package com.projet.tournoi.web;

import java.util.Collection;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.projet.tournoi.entities.Matche;
import com.projet.tournoi.service.fd.MatchService;



@RestController
public class MatchController {
	@Autowired
	  MatchService matchservice;
		
	  @GetMapping("/match/{id}")
	  public Matche getmatch(@PathVariable Long id) {
		  return matchservice.getMatch(id);
		 
	 }
	  
	  @GetMapping("/match")
	  public Collection<Matche> getmatchs(){
		  return matchservice.getAll();
	  }
	  
	  @PostMapping("/match")
	  public Matche addmatch(@RequestBody Matche M) {
		  return matchservice.ajoutertMatch(M);
	  }
	  
	  @PutMapping("/match")
	  public Matche updatematch(@RequestBody Matche M) {
		 return matchservice.modifierMatch(M);
	  }
	  
	  @DeleteMapping("/match/{id}")
	  public void deleltematch(@PathVariable Long id) {
		  matchservice.supprimerMatche(id);
	  }

}
