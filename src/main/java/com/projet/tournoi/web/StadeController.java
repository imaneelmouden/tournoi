package com.projet.tournoi.web;

import java.util.Collection;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.projet.tournoi.entities.Stade;
import com.projet.tournoi.service.fd.StadeService;

@RestController
public class StadeController {
	
	@Autowired
	  StadeService stadeservice;
		
	  @GetMapping("/stade/{id}")
	  public Stade getstade(@PathVariable Long id) {
		  return stadeservice.getStade(id);
		 
	 }
	  
	  @GetMapping("/stade")
	  public Collection<Stade> getstades(){
		  return stadeservice.getAll();
	  }
	  
	  @PostMapping("/stade")
	  public Stade addstade(@RequestBody Stade S) {
		  return stadeservice.ajoutertStade(S);
	  }
	  
	  @PutMapping("/stade")
	  public Stade updatestade(@RequestBody Stade S) {
		 return stadeservice.modifierStade(S);
	  }
	  
	  @DeleteMapping("/stade/{id}")
	  public void deleltestade(@PathVariable Long id) {
		  stadeservice.supprimerStade(id);
	  }


}
